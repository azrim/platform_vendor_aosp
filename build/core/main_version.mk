# Build fingerprint
ifeq ($(BUILD_FINGERPRINT),)
BUILD_NUMBER_NORTICAL := $(shell date -u +%H%M)
NORTICAL_DEVICE ?= $(TARGET_DEVICE)
ifneq ($(filter RELEASE,$(NORTICAL_BUILD_TYPE)),)
BUILD_SIGNATURE_KEYS := release-keys
else
BUILD_SIGNATURE_KEYS := test-keys
endif
BUILD_FINGERPRINT := $(PRODUCT_BRAND)/$(NORTICAL_DEVICE)/$(NORTICAL_DEVICE):$(PLATFORM_VERSION)/$(BUILD_ID)/$(BUILD_NUMBER_NORTICAL):$(TARGET_BUILD_VARIANT)/$(BUILD_SIGNATURE_KEYS)
endif
ADDITIONAL_BUILD_PROPERTIES += \
    ro.build.fingerprint=$(BUILD_FINGERPRINT)

# AOSP recovery flashing
ifeq ($(TARGET_USES_AOSP_RECOVERY),true)
ADDITIONAL_BUILD_PROPERTIES += \
    persist.sys.recovery_update=true
endif

# Branding
NORTICAL_DATE_YEAR := $(shell date -u +%Y)
NORTICAL_DATE_MONTH := $(shell date -u +%m)
NORTICAL_DATE_DAY := $(shell date -u +%d)
NORTICAL_DATE_HOUR := $(shell date -u +%H)
NORTICAL_DATE_MINUTE := $(shell date -u +%M)
NORTICAL_BUILD_DATE_UTC := $(shell date -d '$(NORTICAL_DATE_YEAR)-$(NORTICAL_DATE_MONTH)-$(NORTICAL_DATE_DAY) $(NORTICAL_DATE_HOUR):$(NORTICAL_DATE_MINUTE) UTC' +%s)
NORTICAL_BUILD_DATE := $(NORTICAL_DATE_YEAR)$(NORTICAL_DATE_MONTH)$(NORTICAL_DATE_DAY)-$(NORTICAL_DATE_HOUR)$(NORTICAL_DATE_MINUTE)

NORTICAL_PLATFORM_VERSION := 11.0
NORTICAL_CODENAME := Alexandrite

TARGET_PRODUCT_SHORT := $(subst aosp_,,$(NORTICAL_BUILD))

NORTICAL_VERSION := Nortical-UI_$(NORTICAL_BUILD)-$(TARGET_BUILD_VARIANT)-$(NORTICAL_PLATFORM_VERSION)-$(NORTICAL_CODENAME)-$(NORTICAL_BUILD_DATE)

ADDITIONAL_BUILD_PROPERTIES += \
    org.nortical.version.prop=$(NORTICAL_VERSION_PROP) \
    org.nortical.build_date_utc=$(NORTICAL_BUILD_DATE_UTC) \
    org.nortical.device=$(TARGET_DEVICE) \
    org.nortical.ota.version_code=11-R \
    org.nortical.codename=$(NORTICAL_CODENAME) \
    org.nortical.version.prop=$(PLATFORM_VERSION) \
    org.nortical.build_version=$(NORTICAL_VERSION) \
    org.nortical.ota_support=true
